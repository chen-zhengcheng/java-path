import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class OffsetTimeConverter {
    private static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
    public static void main(String[] args) {
        Calendar date = new GregorianCalendar();
        date.set(2022, 5, 01, 12, 00);
        convertCutOverTimeToSydneyTime(date, null);
    }

    public static void convertCutOverTimeToSydneyTime(Calendar date, String timeZone) {
        System.out.println(date.toInstant().toString());
        date.setTimeZone(TimeZone.getTimeZone("Australia/Perth"));
        System.out.println(date.toInstant().toString());

        ZonedDateTime perthZonedDateTime = ZonedDateTime.of(LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("Australia/Perth")), ZoneId.of("Australia/Perth"));
        System.out.println(perthZonedDateTime.toLocalDateTime());

        ZonedDateTime sydZonedDateTime = ZonedDateTime.of(LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("Australia/Perth")), ZoneId.of("Australia/Sydney"));
        System.out.println(sydZonedDateTime.toLocalDateTime());

        ZoneOffset sydneyZoneOffset = ZoneId.of("Australia/Sydney").getRules().getOffset(perthZonedDateTime.toInstant());
        System.out.println(sydneyZoneOffset);

        OffsetDateTime offsetDateTime = perthZonedDateTime.withZoneSameInstant(ZoneId.of("Australia/Sydney")).toOffsetDateTime();
        System.out.println(offsetDateTime);

        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(fmt.format(offsetDateTime));
    }

    public static String convertTimeZoneOptionToZoneId(String timeZone) {
        return "Australia/" + timeZone.split("/")[0];
    }
}